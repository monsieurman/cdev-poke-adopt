import { Component } from '@angular/core';
import { PokeApiService } from './poke-api.service';

@Component({
  selector: 'app-root',
  template: `
    <app-header></app-header>
  `
})
export class AppComponent {
  title = 'poke-adopt2';
  
  constructor(
    private pokeApi: PokeApiService
  ) {
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokeApiService {
  constructor(
    private httpClient: HttpClient
  ) { 
    this.httpClient.get(`https://pokeapi.co/api/v2/pokemon/`)
      .subscribe(console.log);
  }
}
